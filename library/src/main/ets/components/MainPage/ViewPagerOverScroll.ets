/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

@ComponentV2
struct ViewPagerOverScroll {
  @Param @Require model: ViewPagerOverScroll.Model
  @BuilderParam child: () => ESObject

  build() {
    Flex({ alignItems: ItemAlign.Center, justifyContent: FlexAlign.Center }) {
      this.child()
    }.height(this.model.mHeight)
    .width(this.model.mWidth)
  }
}

namespace ViewPagerOverScroll {
  @ObservedV2
  export class Model {
    @Trace mHeight: number | string = px2vp(2340)
    @Trace mWidth: number | string = px2vp(lpx2px(720))

    public getHeight(): number | string {
      return this.mHeight
    }

    public setHeight(height: number | string): Model {
      this.mHeight = height
      return this
    }

    public getWidth(): number | string {
      return this.mWidth
    }

    public setWidth(width: number | string): Model {
      this.mWidth = width
      return this
    }
  }
}

export default ViewPagerOverScroll