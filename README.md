# overscroll-decor

## Introduction
> overscroll-decor provides an iOS-like over-scrolling effect to scrollable views.
> The following views are supported: RecyclerView, ListView, GridView, ViewPager, ScrollView, HorizontalScrollView, Any View - Text, Image ...

![](screenshot/screenshot.gif)

## How to Install
```shell
ohpm install @ohos/overscroll-decor
```
For details, see [Installing an OpenHarmony HAR](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.en.md).

## Usage

### The following demonstrates how to implement the over-scrolling effect of GridView. The implementation is the same for other views. 
### 1. Instantiate an **OverScrollDecor.Model** object.
 ```
private model: OverScrollDecor.Model = new OverScrollDecor.Model()
 ```
### 2. Set UI attributes through a **Model** object to define the required style.
 ```
private aboutToAppear() {
    this.model
      .setUpOverScroll(true)
      .setOrientation(OverScrollDecor.ORIENTATION.VERTICAL)
      .setOverScrollBounceEffect(true)
      .setScrollBar(true)
      .setWidth("100%")
      .setHeight("80%")
  }
 ```
### 3. Draw the subcomponent.
 ```
@Builder SpecificChild() {
    Column({ space: 10 }) {
      ......
    }.width('100%')
  }
 ```
### 4. Draw the UI.
 ```
build() {
  Stack({ alignContent: Alignment.TopStart }) {      
  ......
  OverScrollDecor({ model: this.model, child: () => { this.SpecificChild() } })
  ......
}
 ```
For details, see the implementation on the sample page of the open-source library.

## UI Attributes
1. **mHeight**: height of the scrollable component. The default value is **px2vp(2340)**.<br>
   `mHeight: number | string = px2vp(2340)`
2. **mWidth**: height of the scrollable component. The default value is **px2vp(lpx2px(720))**.<br>
   `mWidth: number | string = px2vp(lpx2px(720))`
3. **mMargin**: margin of the scrollable component. The default value is **16**.<br>
   `mMargin: number = 16`
4. **mOrientation**: orientation of the scrollable component within the UI. The default value is **VERTICAL**.
   `mOrientation:  ORIENTATION = 0`
5. **mScrollBar**: whether to use a scroll bar. The default value is true.
   `mScrollBar: boolean = true`
6. **mOverScrollBounceEffect**: whether to enable the overscroll bounce effect. The default value is **false**.
   `mOverScrollBounceEffect: boolean = false`
7. **mUpOverScroll**: whether to enable the upward overscroll behavior. The default value is **true**.
   `mUpOverScroll: boolean = true`

## Constraints
overscroll-decor has been verified only in the following version:

Deveco Studio:4.0 (4.0.3.513),SDK:API10 (4.0.10.10)
DevEco Studio: NEXT Beta1-5.0.3.806, SDK: API12 Release (5.0.0.66)

## Directory Structure
````
|---- overscroll-decor 
|     |---- entry  # Sample code
| |---- library # Overscroll library
|           |---- index.ets  # External APIs
|           |---- src
|                  |---- main
|                        |---- ets
|                              |---- components
|                                    |---- OverScrollDecor.ets # Implementation of the scrolling functionality
|     |---- README_EN.md  # Brief introduction of the overscroll-decor library                   
````

## How to Contribute
If you find any problem during the use, submit an [Issue](https://gitee.com/openharmony-sig/overscroll-decor/issues) or [PR](https://gitee.com/openharmony-sig/overscroll-decor/pulls) to us.

## License
This project is licensed under [Apache License 2.0](https://gitee.com/openharmony-sig/overscroll-decor/blob/master/LICENSE).
